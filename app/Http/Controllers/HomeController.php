<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    public static function personName($listId) {
        return $name = str_replace(
            '-',
            ' ',
            title_case($listId)
        );
    }

    public static function listId($filename) {
        return $listId = collect(explode('.', basename($filename)))->first();
    }

    public function index() {
        $datas = Storage::disk('persons')->files('.');
        
        $persons = collect($datas)
            ->filter(function($data) {
                $spreadFileName = collect(explode('.', basename($data)));
                if($spreadFileName->count() !== 2) {
                    return false;
                }

                $name = $spreadFileName->first();
                $type = $spreadFileName->last();

                return $type === 'json' && $name !== 'nama-lengkap';
            })
            ->map(function($data) {
                $person = json_decode(Storage::disk('persons')->get($data), true);
                $listId = self::listId($data);
                $name = self::personName($listId);

                return array_merge($person, [
                    'name' => $name,
                    'list_id' => $listId,
                ]);
            })
            ->toArray();

        return view('home', compact('persons'));
    }
}
