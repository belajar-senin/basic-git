<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <link rel="stylesheet" href="/css/app.css">
    </head>
    <body>
        <div class="container mt-4">
            <div class="row">
                <div class="col-4">
                    <div class="list-group" id="list-tab" role="tablist">
                        @foreach ($persons as $key => $person)
                            <a 
                            class="list-group-item list-group-item-action {{ $key === 0 ? 'active' : '' }}"
                            id="{{ $person['list_id'] }}-list"
                            data-toggle="list"
                            href="#{{ $person['list_id'] }}"
                            role="tab"
                            aria-controls="home">
                                {{ $person['name'] }}
                            </a>
                        @endforeach
                    </div>
                </div>
                <div class="col-8">
                    <div class="tab-content" id="nav-tabContent">
                        @foreach ($persons as $key => $person)
                            <div
                            class="tab-pane fade show {{ $key === 0 ? 'active' : '' }}"
                            id="{{ $person['list_id'] }}"
                            role="tabpanel"
                            aria-labelledby="{{ $person['list_id'] }}-list">
                                <h4 class="mb-0">Bio:</h4>
                                <hr class="my-2">
                                <p>{{ $person['bio'] }}</p>
                                <br>
                                <h4 class="mb-0">Testismonials:</h4>
                                <hr class="my-2">
                                @if (count($person['testimonials']) > 0)
                                    <ul class="list-unstyled">
                                        @foreach ($person['testimonials'] as $testimonial)
                                        <li class="media my-4">
                                            <div class="media-body">
                                                <h5 class="mt-0">{{ $testimonial['name'] }}</h5>
                                                {{ $testimonial['testimonial'] }}
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <script src="/js/app.js"></script>
    </body>
</html>
